# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2020-present Linaro Limited
#
# Author: Rémi Duraffort <remi.duraffort@linaro.org>
#
# SPDX-License-Identifier: MIT

from dataclasses import dataclass
from pathlib import Path
import os
import shlex
import subprocess
import sys
import time

import requests


#############
# Constants #
#############
@dataclass
class Colors:
    blue: str = "\x1b[1;34;40m"
    purple: str = "\x1b[1;35;40m"
    red: str = "\x1b[1;31;40m"
    white: str = "\x1b[1;37;40m"
    yellow: str = "\x1b[1;33;40m"
    reset: str = "\x1b[0m"


if sys.stdout.isatty():
    COLORS = Colors()
else:
    COLORS = Colors("", "", "", "", "", "")

CODENAMES = ["stretch", "buster", "bullseye", "sid"]
TIMEOUT = 10
URL = "https://cloud.debian.org/images/cloud/"


###########
# Helpers #
###########
def run(cmd: str) -> None:
    print(f"{COLORS.blue}$ {Colors.white}{cmd}{Colors.reset}")

    ret = subprocess.call(shlex.split(cmd))
    if ret != 0:
        raise Exception("Unable to run '%s', returned %d" % (cmd, ret))


def submit(url: str, token: str, definition: str) -> int:
    headers = {"Authorization": f"Token {token}"}
    data = requests.post(
        f"{url}/api/v0.2/jobs/", json={"definition": definition}, headers=headers
    )
    return data.json()["job_ids"][0]


def wait(url: str, token: str, job_id: int) -> str:
    headers = {"Authorization": f"Token {token}"}
    while True:
        data = requests.get(f"{url}/api/v0.2/jobs/{job_id}/", headers=headers).json()
        if data["state"] == "Finished":
            return data["health"]
        time.sleep(5)


def push_to_artifactorial(url: str, token: str, filename: str) -> str:
    path = Path(filename)
    ret = requests.post(
        url, data={"token": token}, files={"path": (path.name, path.read_bytes())}
    )
    ret.raise_for_status()
    return ret.content.decode("utf-8")


##############
# Entrypoint #
##############
def main() -> int:
    pwd = Path(".").resolve()
    job = (pwd / "share" / "lava-job.yaml").read_text(encoding="utf-8")

    # Get LAVA credentials from environment
    LAVA_URL = os.environ.get("LAVA_URL")
    LAVA_TOKEN = os.environ.get("LAVA_TOKEN")

    # Grab artifactorial if available
    ARTIFACTORIAL_URL = os.environ.get("ARTIFACTORIAL_URL")
    ARTIFACTORIAL_TOKEN = os.environ.get("ARTIFACTORIAL_TOKEN")

    jobs = {}
    for codename in CODENAMES:
        print("#################")
        print(f"# {codename}")
        run(f"python3 debian.py {codename}")

        if ARTIFACTORIAL_URL and ARTIFACTORIAL_TOKEN:
            print("Push to artifactorial")
            url = push_to_artifactorial(
                ARTIFACTORIAL_URL,
                ARTIFACTORIAL_TOKEN,
                f"output/debian-{codename}.qcow2.zst",
            )
            print(f"-> {url}")
        else:
            url = f"file://{pwd}/output/debian-{codename}.qcow2.zst"

        if LAVA_URL and LAVA_TOKEN:
            print("Submit lava job")
            definition = job.format(codename=codename, url=url)
            job_id = submit(LAVA_URL, LAVA_TOKEN, definition)
            if job_id:
                jobs[codename] = job_id
            print(f"-> {job_id}")
            print("#################\n")

    if not jobs:
        return 0

    print("#################")
    print("Waiting for results:")
    errors = 0
    for codename in CODENAMES:
        print(f"-> {COLORS.blue}{codename}{COLORS.reset} ({job_id})")
        health = wait(LAVA_URL, LAVA_TOKEN, jobs[codename])
        if health == "Complete":
            print(f" --> {COLORS.blue}{health}{COLORS.reset}")
        else:
            errors += 1
            print(f" --> {COLORS.red}{health}{COLORS.reset}")
    return errors


if __name__ == "__main__":
    sys.exit(main())
